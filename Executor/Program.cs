﻿namespace Executor
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using ILRepacking;
    using Newtonsoft.Json;

    class Program
    {
        static void Main(string[] args)
        {
            var logger = new Logger
            {
                ShouldLogVerbose = true
            };

            if (!logger.Open("./logger.txt"))
                throw new Exception("failed log");

            var outputPathFile = Path.Combine(Environment.CurrentDirectory, "output.dll");

            var options = new RepackOptions()
            {
                Log = true,
                SearchDirectories = new List<string>() { "." },
                InputAssemblies = new[] { "Newtonsoft.Json.dll" }.Select(x => Path.Combine( Environment.CurrentDirectory, x )).ToArray(),
                OutputFile = outputPathFile,
                TargetKind = ILRepack.Kind.Dll
            };

            // Attempt to create output directory if it does not exist.
            var outputPath = Path.GetDirectoryName(outputPathFile);
            if (outputPath != null && !Directory.Exists(outputPath))
                    Directory.CreateDirectory(outputPath);

            var a = JsonConvert.SerializeObject("abc");

            var ilRepack = new ILRepack(options, logger);
            ilRepack.Repack();
        }
    }
}
